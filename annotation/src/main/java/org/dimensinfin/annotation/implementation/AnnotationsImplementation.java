package org.dimensinfin.annotation.implementation;

import java.text.MessageFormat;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import org.dimensinfin.logging.LogWrapper;

@Aspect
public class AnnotationsImplementation {
	@Around("execution(* *(..)) && @annotation(LogEnterExit)")
	public Object annotationLogEnterExit( ProceedingJoinPoint point ) throws Throwable {
		LogWrapper.enter();
		final Object result = point.proceed();
		LogWrapper.exit();
		return result;
	}

	@Around("execution(* *(..)) && @annotation(TimeElapsed)")
	public Object around( ProceedingJoinPoint point ) throws Throwable {
		final long start = System.currentTimeMillis();
		final Object result = point.proceed();
		LogWrapper.exit( MessageFormat.format( "{0,number,integer}ms", System.currentTimeMillis() - start ) );
		return result;
	}
}