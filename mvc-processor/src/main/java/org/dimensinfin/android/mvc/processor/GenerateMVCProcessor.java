package org.dimensinfin.android.mvc.processor;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Elements;
import javax.tools.Diagnostic;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import org.dimensinfin.android.mvc.annotations.BindField;
import org.dimensinfin.android.mvc.annotations.GenerateMVC;
import org.dimensinfin.android.mvc.domain.IRender;
import org.dimensinfin.annotation.Keep;

public class GenerateMVCProcessor extends AbstractProcessor {
	public static Set<TypeElement> getTypeElementsToProcess( Set<? extends Element> elements,
	                                                         Set<? extends Element> supportedAnnotations ) {
		Set<TypeElement> typeElements = new HashSet<>();
		for (Element element : elements) {
			if (element instanceof TypeElement) {
				boolean found = false;
				final List<? extends AnnotationMirror> annotations = element.getAnnotationMirrors();

				for (Element subElement : element.getEnclosedElements()) {
					for (AnnotationMirror mirror : subElement.getAnnotationMirrors()) {
						for (Element annotation : supportedAnnotations) {
							if (mirror.getAnnotationType().asElement().equals( annotation )) {
								typeElements.add( (TypeElement) element );
								found = true;
								break;
							}
						}
						if (found) break;
					}
					if (found) break;
				}
			}
		}
		return typeElements;
	}

	private Filer filer;
	private Messager messager;
	private Elements elementUtils;

	@Override
	public Set<String> getSupportedAnnotationTypes() {
		return new TreeSet<>( Arrays.asList(
				GenerateMVC.class.getCanonicalName(),
				BindField.class.getCanonicalName(),
				Keep.class.getCanonicalName() ) );
	}

	@Override
	public synchronized void init( final ProcessingEnvironment processingEnv ) {
		super.init( processingEnv );
		this.filer = processingEnv.getFiler();
		this.messager = processingEnv.getMessager();
		this.elementUtils = processingEnv.getElementUtils();
	}

	@Override
	public boolean process( Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment ) {
		if (!roundEnvironment.processingOver()) {
			// Get the model classes tagged with generation to buold the base classes
			final Set<TypeElement> typeElements = (Set<TypeElement>) roundEnvironment.getElementsAnnotatedWith( GenerateMVC.class );
			for (TypeElement typeElement : typeElements) {
				this.generateRenderClass( typeElement );
				this.generateControllerClass( typeElement );
			}
		}
		return true;
	}

	private void generateControllerClass( final TypeElement typeElement ) {
		// - N A M E S
		final String packageName = elementUtils.getPackageOf( typeElement ).getQualifiedName().toString();
		final String typeName = typeElement.getSimpleName().toString();
		final ClassName modelClassName = ClassName.get( packageName, typeName );

		final ClassName controllerClassName = ClassName.get( packageName, MVCNameStore.getGeneratedControllerClassName( typeName ) );
		final ClassName renderClassName = ClassName.get( packageName, MVCNameStore.getGeneratedRenderClassName( typeName ) );
		final ParameterizedTypeName superAndroidControllerClassName = ParameterizedTypeName.get(
				MVCNameStore.generateAndroidControllerClassName(),
				TypeVariableName.get( typeName )
		);
		// - C L A S S
		TypeSpec.Builder classBuilder = TypeSpec.classBuilder( controllerClassName )
				                                .addModifiers( Modifier.PUBLIC )
				                                .superclass( superAndroidControllerClassName )
				                                .addAnnotation( Keep.class );
		final GenerateMVC generateMVCTag = typeElement.getAnnotation( GenerateMVC.class );
		if (generateMVCTag != null) {
			if (generateMVCTag.onClickFeature())
				classBuilder = TypeSpec.classBuilder( controllerClassName )
						               .addModifiers( Modifier.PUBLIC, Modifier.ABSTRACT )
						               .superclass( superAndroidControllerClassName )
						               .addSuperinterface( View.OnClickListener.class )
						               .addAnnotation( Keep.class );
		}
		// - C O N S T R U C T O R
		classBuilder.addMethod( MethodSpec.constructorBuilder()
				                        .addModifiers( Modifier.PUBLIC )
				                        .addParameter( modelClassName, MVCNameStore.Parameter.CONSTRUCTOR_MODEL, Modifier.FINAL )
				                        .addParameter( MVCNameStore.generateIControllerFactoryClassName(),
						                        MVCNameStore.Parameter.CONSTRUCTOR_FACTORY,
						                        Modifier.FINAL )
				                        .addStatement( "super($N, $N)",
						                        MVCNameStore.Parameter.CONSTRUCTOR_MODEL,
						                        MVCNameStore.Parameter.CONSTRUCTOR_FACTORY )
				                        .build() );

		// - A N D R O I D C O N T R O L L E R
		classBuilder.addMethod( MethodSpec.methodBuilder( MVCNameStore.Method.BUILD_RENDER )
				                        .addModifiers( Modifier.PUBLIC )
				                        .returns( IRender.class )
				                        .addParameter( Context.class, MVCNameStore.Parameter.BUILDRENDER_CONTEXT )
				                        .addAnnotation( Override.class )
				                        .addStatement( "return new $N( this, context )", renderClassName.simpleName() )
				                        .build()
		);
		// - V I E W . O N C L I C K L I S T E N E R
//		if (generateMVCTag != null) {
//			if (generateMVCTag.onClickFeature())
//				classBuilder.addMethod( MethodSpec.methodBuilder( MVCNameStore.Method.ANDROID_VIEW_ONCLICK )
//						                        .addModifiers( Modifier.PUBLIC, Modifier.ABSTRACT )
//						                        .returns( void.class )
//						                        .addParameter( View.class, MVCNameStore.Parameter.ONCLICK_VIEW, Modifier.FINAL )
//						                        .addAnnotation( Override.class )
//						                        .build()
//				);
//		}
		// write the defines class to a java file
		try {
			JavaFile.builder( packageName,
					classBuilder.build() )
					.build()
					.writeTo( filer );
		} catch (IOException e) {
			messager.printMessage( Diagnostic.Kind.ERROR, e.toString(), typeElement );
		}
	}

	private void generateRenderClass( final TypeElement typeElement ) {
		// - N A M E S
		final String packageName = this.elementUtils.getPackageOf( typeElement ).getQualifiedName().toString();
		final String typeName = typeElement.getSimpleName().toString();

		final ClassName controllerClassName = ClassName.get( packageName, MVCNameStore.getGeneratedControllerClassName( typeName ) );
		final ClassName renderClassName = ClassName.get( packageName, MVCNameStore.getGeneratedRenderClassName( typeName ) );
		final ParameterizedTypeName superRenderClassName = ParameterizedTypeName.get(
				MVCNameStore.classTypedRender(),
				TypeVariableName.get( MVCNameStore.getGeneratedControllerClassName( typeName ) )
		);
		// - C L A S S
		final TypeSpec.Builder classBuilder = TypeSpec.classBuilder( renderClassName )
				                                      .addModifiers( Modifier.PUBLIC )
				                                      .superclass( superRenderClassName )
				                                      .addAnnotation( Keep.class );
		// - C O N S T R U C T O R
		classBuilder.addMethod( MethodSpec.constructorBuilder()
				                        .addModifiers( Modifier.PUBLIC )
				                        .addParameter( controllerClassName, MVCNameStore.Field.CONTROLLER, Modifier.FINAL )
				                        .addParameter( Context.class, MVCNameStore.Field.ANDROID_CONTEXT, Modifier.FINAL )
				                        .addStatement( "super($N, $N)",
						                        MVCNameStore.Field.CONTROLLER,
						                        MVCNameStore.Field.ANDROID_CONTEXT )
				                        .build() );
		// - F I E L D S
		for (VariableElement variableElement : ElementFilter.fieldsIn( typeElement.getEnclosedElements() )) {
			BindField bindFieldTag = variableElement.getAnnotation( BindField.class );
			if (bindFieldTag != null) {
				classBuilder.addField(
						FieldSpec
								.builder(
										TextView.class, variableElement.getSimpleName().toString() )
								.addModifiers( Modifier.PROTECTED )
								.build()
				);
			}
		}
		// - I R E N D E R
		final GenerateMVC generateMVCTag = typeElement.getAnnotation( GenerateMVC.class );
		classBuilder.addMethod( MethodSpec.methodBuilder( MVCNameStore.Method.IRENDER_ACCESS_LAYOUT_REFERENCE )
				                        .addModifiers( Modifier.PUBLIC )
				                        .returns( int.class )
				                        .addAnnotation( Override.class )
				                        .addStatement( "return $L", generateMVCTag.layout() )
				                        .build()
		);
		final MethodSpec.Builder initializeViewsMethod = MethodSpec.methodBuilder( MVCNameStore.Method.IRENDER_INITIALIZE_VIEWS )
				                                                 .addModifiers( Modifier.PUBLIC )
				                                                 .returns( void.class )
				                                                 .addAnnotation( Override.class );
		for (VariableElement variableElement : ElementFilter.fieldsIn( typeElement.getEnclosedElements() )) {
			BindField bindFieldTag = variableElement.getAnnotation( BindField.class );
			if (bindFieldTag != null)
				initializeViewsMethod.addStatement( "this.$N = ($T) $T.requireNonNull( this.getView().findViewById( $L ) )",
						variableElement.getSimpleName().toString(),
						TextView.class,
						Objects.class,
						generateMVCTag.layout()
				);
		}
		classBuilder.addMethod( initializeViewsMethod.build() );
		final MethodSpec.Builder updateContentMethod =MethodSpec.methodBuilder( MVCNameStore.Method.IRENDER_UPDATE_CONTENT)
				                        .addModifiers( Modifier.PUBLIC )
				                        .returns( void.class )
				                        .addAnnotation( Override.class );
		for (VariableElement variableElement : ElementFilter.fieldsIn( typeElement.getEnclosedElements() )) {
			BindField bindFieldTag = variableElement.getAnnotation( BindField.class );
			if (bindFieldTag != null)
				updateContentMethod.addStatement( "this.$N.setText( this.getController().getModel().$L() )",
						variableElement.getSimpleName().toString(),
						"get"+this.capitalize(variableElement.getSimpleName().toString())
				);
		}
		classBuilder.addMethod( updateContentMethod.build() );

		// - B U I L D E R
//		classBuilder.addType( this.generateRenderBuilder( typeElement ) );

		try {
			JavaFile.builder( packageName,
					classBuilder.build() )
					.build()
					.writeTo( filer );
		} catch (IOException e) {
			messager.printMessage( Diagnostic.Kind.ERROR, e.toString(), typeElement );
		}
	}
	private  String capitalize(final String str) {
		if(str == null || str.isEmpty()) {
			return str;
		}
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	private TypeSpec generateRenderBuilder( final TypeElement typeElement ) {
		// - N A M E S
		final String packageName = this.elementUtils.getPackageOf( typeElement ).getQualifiedName().toString();
		final String typeName = typeElement.getSimpleName().toString();
		final ClassName className = ClassName.get( packageName, typeName );
		final ClassName builderClassName = MVCNameStore.classBuilder();

		// - C L A S S
		final TypeSpec.Builder classBuilder = TypeSpec.classBuilder( builderClassName )
				                                      .addModifiers( Modifier.PUBLIC, Modifier.STATIC );
//				                                      .superclass( superRenderClassName )
//				                                      .addAnnotation( Keep.class );
		// - F I E L D S
		classBuilder.addField(
				FieldSpec.builder( className, "onConstruction" )
						.addModifiers( Modifier.PRIVATE )
						.build()
		);
		// - C O N S T R U C T O R
		classBuilder.addMethod( MethodSpec.constructorBuilder()
				                        .addModifiers( Modifier.PUBLIC )
				                        .addStatement( "this.onConstruction = new $T()", className )
				                        .build() );
		// - B U I L D
		classBuilder.addMethod( MethodSpec.methodBuilder( MVCNameStore.Method.BUILDER_BUILD )
				                        .addModifiers( Modifier.PUBLIC )
				                        .returns( className )
				                        .addStatement( "return this.onConstruction", className )
				                        .build()
		);

		return classBuilder.build();
	}
}
