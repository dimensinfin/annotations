package org.dimensinfin.android.mvc.processor;

import com.squareup.javapoet.ClassName;

public final class MVCNameStore {
	private static final String GENERATED_SUFFIX = "$Base";
	private static final String CONTROLLER_SUFFIX = "Controller";
	private static final String RENDER_SUFFIX = "Render";
	//	private static final String ANDROID_CONTROLLER_PACKAGE = "org.dimensinfin.android.mvc.controller";
	private static final String ANDROID_CONTROLLER_NAME = "AndroidController";
	//	private static final String ICONTROLLER_FACTORY_PACKAGE="org.dimensinfin.android.mvc.factory";
	private static final String ICONTROLLER_FACTORY_NAME = "IControllerFactory";
	//	private static final String LOGGER_PACKAGE_NAME = "org.dimensinfin.android.mvc.annotations.logging";
//	private static final String LOGGER_WRAPPER = "LoggerWrapper";


//	private static final String ONCLICK_FEATURE = "View.OnClickListener";

	public static String getGeneratedControllerClassName( String clsName ) {
		return clsName + CONTROLLER_SUFFIX + GENERATED_SUFFIX;
	}

	public static ClassName generateAndroidControllerClassName() {
		return ClassName.get( Package.ANDROID_CONTROLLER_PACKAGE, ANDROID_CONTROLLER_NAME );
	}

	public static ClassName generateIControllerFactoryClassName() {
		return ClassName.get( Package.ICONTROLLER_FACTORY_PACKAGE, ICONTROLLER_FACTORY_NAME );
	}

	public static String getGeneratedRenderClassName( final String className ) {
		return className + RENDER_SUFFIX + GENERATED_SUFFIX;
	}

	public static String getGeneratedFinalRenderClassName( final String className ) {
		return className + RENDER_SUFFIX;
	}

	public static ClassName classTypedRender() {
		return ClassName.get( Package.TYPED_RENDER_PACKAGE, Class.TYPED_RENDER );
	}

	public static ClassName classBuilder() {
		return ClassName.get( "", Class.BUILDER );
	}

	private MVCNameStore() { }

//	public static ClassName getLoggerClassName() {
//		return ClassName.get( LOGGER_PACKAGE_NAME, LOGGER_WRAPPER );
//	}

	public static class Package {
		public static final String TYPED_RENDER_PACKAGE = "org.dimensinfin.android.mvc.render";
		public static final String ANDROID_CONTROLLER_PACKAGE = "org.dimensinfin.android.mvc.controller";
		public static final String ICONTROLLER_FACTORY_PACKAGE = "org.dimensinfin.android.mvc.factory";
	}

	public static class Class {
		public static final String TYPED_RENDER = "TypedRender";
		public static final String BUILDER = "Builder";
	}

	public static class Method {
		public static final String BUILD_RENDER = "buildRender";
		public static final String ANDROID_VIEW_ONCLICK = "onClick";
		public static final String IRENDER_ACCESS_LAYOUT_REFERENCE = "accessLayoutReference";
		public static final String IRENDER_INITIALIZE_VIEWS = "initializeViews";
		public static final String IRENDER_UPDATE_CONTENT = "updateContent";
		public static final String BUILDER_BUILD = "build";
	}

	public static class Variable {
		public static final String CONTROLLER_MODEL = "model";
		public static final String CONTROLLER_FACTORY = "factory";
		public static final String BUILD_CONTEXT = "context";
		public static final String ONCLICK_VIEW = "view";

		public static final String ANDROID_ACTIVITY = "activity";
		public static final String ANDROID_VIEW = "view";
	}

	public static class Field {
		public static final String CONTROLLER = "controller";
		public static final String ANDROID_CONTEXT = "context";
	}

	public static class Parameter {
		public static final String CONSTRUCTOR_MODEL = "model";
		public static final String CONSTRUCTOR_FACTORY = "factory";
		public static final String BUILDRENDER_CONTEXT = "context";
		public static final String ONCLICK_VIEW = "view";
	}
}

