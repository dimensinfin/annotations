package org.dimensinfin.android.mvc.controller;

import android.content.Context;

import org.dimensinfin.android.mvc.domain.IRender;
import org.dimensinfin.core.interfaces.IEventEmitter;

public interface IAndroidController<M> extends IEventEmitter, Comparable {
	M getModel();

	// - A B S T R A C T
	IRender buildRender( final Context context );

}
