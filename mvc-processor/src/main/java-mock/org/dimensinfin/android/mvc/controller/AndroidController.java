package org.dimensinfin.android.mvc.controller;

import androidx.annotation.NonNull;

import java.util.Objects;

import org.dimensinfin.android.mvc.factory.IControllerFactory;
import org.dimensinfin.core.interfaces.ICollaboration;

/**
 * This class will implement the core Android interaction controller on the classic pattern Model-View-Controller
 * special and generic implementation into a library for Android native projects. This controller pattern will be the
 * intermediate connection node between the free graph composition of the Model and the final Visual Controller list
 * that will connect the Model and the Render inside the View containers used in the Android interface.
 *
 * On version 4.0 I have replaced the old GEF Part concept by the traditional Controller and started to add unit test
 * code and replaced old style java code practices by the new advanced code techniques and refactorings.
 *
 * @author Adam Antinoo
 * @since 4.0.0
 */
public abstract class AndroidController<M extends ICollaboration> implements IAndroidController<M> {
	private final IControllerFactory factory;
	private ControllerAdapter<M> delegatedController;

	// - C O N S T R U C T O R S
	private AndroidController( @NonNull final ControllerAdapter<M> delegate, @NonNull final IControllerFactory factory ) {
		this.delegatedController = Objects.requireNonNull( delegate );
		this.factory = Objects.requireNonNull( factory );
	}

	public AndroidController( @NonNull final M model, @NonNull final IControllerFactory factory ) {
		this( new ControllerAdapter<M>( model ), factory );
	}

	@Override
	public M getModel() {
		return this.delegatedController.getModel();
	}

	@Override
	public int compareTo( @NonNull final Object target ) {
		if (target instanceof IAndroidController) {
			final IAndroidController castedTarget = (IAndroidController) target;
			return this.getModel().compareTo( castedTarget.getModel() );
		} else return -1;
	}
}
