package org.dimensinfin.android.mvc.controller;

import androidx.annotation.NonNull;

import java.util.Objects;

import org.dimensinfin.core.interfaces.ICollaboration;
import org.dimensinfin.core.interfaces.IEventEmitter;

/**
 * Delegate to be used on the controllers to isolate from the model class to be used on the controller. With this
 * delegate we are able to isolate the model and be able to use inheritance for development of controllers.
 *
 * @author Adam Antinoo
 */

public class ControllerAdapter<M extends ICollaboration> implements IEventEmitter, Comparable<M> {
	// - F I E L D - S E C T I O N
	/** Reference to the Model node. */
	private final M model; // Holds the model node.

	public M getModel() {
		return this.model;
	}

	public ControllerAdapter( @NonNull final M model ) {
		Objects.requireNonNull( model );
		this.model = model;
	}

	@Override
	public int compareTo( @NonNull final M target ) {
		return this.getModel().compareTo(target);
	}
}
