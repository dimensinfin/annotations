package org.dimensinfin.android.annotations.domain;

import java.util.ArrayList;
import java.util.List;

import org.dimensinfin.android.annotations.R;
import org.dimensinfin.android.mvc.annotations.BindField;
import org.dimensinfin.android.mvc.annotations.GenerateMVC;
import org.dimensinfin.core.interfaces.ICollaboration;

@GenerateMVC(layout = R.layout.actionbar_indeterminateprogress, onClickFeature = false)
public class DomainModel implements ICollaboration {
	@BindField(R.id.bt_1)
	private String tvContent;

	public String getTvContent() {
		return tvContent;
	}

	@Override
	public int compareTo( final Object o ) {
		return 0;
	}

	@Override
	public List<ICollaboration> collaborate2Model( final String variation ) {
		return new ArrayList<>();
	}

//	@OnClick(R.id.bt_1)
//	void bt1Click( View v) {
//		tvContent.setText("Button 1 Clicked");
//	}
}
