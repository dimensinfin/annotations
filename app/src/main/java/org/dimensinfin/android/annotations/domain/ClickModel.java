package org.dimensinfin.android.annotations.domain;

import java.util.ArrayList;
import java.util.List;

import org.dimensinfin.android.annotations.R;
import org.dimensinfin.android.mvc.annotations.BindField;
import org.dimensinfin.android.mvc.annotations.GenerateMVC;
import org.dimensinfin.core.interfaces.ICollaboration;

@GenerateMVC(layout = R.layout.actionbar_indeterminateprogress, onClickFeature = true)
public class ClickModel implements ICollaboration {
	@BindField(R.id.bt_1)
	private String name;
//	@BindField(R.id.bt_1)
	private IIcon icon;

	public String getName() {
		return name;
	}

	@Override
	public int compareTo( final Object o ) {
		return 0;
	}

	@Override
	public List<ICollaboration> collaborate2Model( final String variation ) {
		return new ArrayList<>(  );
	}
}
